import org.junit.Assert;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;


import java.time.LocalDate;


public class PatisserieTest {

	Patisserie p = new Patisserie();
	@Test
	public void getErrorTest() {
		String n = null;
		Assert.assertThat(p.getError(), is(n));
	}
	@Test
	public void isAvailableTest() {
		
		int m = LocalDate.now().getMonthValue();
		if ( m < 5 || m > 7)
			Assert.assertThat(p.isAvailable("cherry"), is(false));
		else
			Assert.assertThat(p.isAvailable("cherry"), is(true));
		if (m < 4 || m > 6)
			Assert.assertThat(p.isAvailable("strawberry"), is(false));
		else 
			Assert.assertThat(p.isAvailable("strawberry"), is(true));
		if (m < 7 || m > 10)
			Assert.assertThat(p.isAvailable("raspberry"), is(false));
		else 
			Assert.assertThat(p.isAvailable("raspberry"), is(true));
	}
	
	@Test
	public void getUstensilsTest() {
		System.out.println(p.getUstensils());
	}
}
