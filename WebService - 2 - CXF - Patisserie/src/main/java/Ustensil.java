import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;


@XmlType(name="Ustensil")
public class Ustensil {
	
	private DataHandler img;
	private String name;
	private int size; //mm
	
	public DataHandler getImg() {
		return img;
	}
	public void setImg(DataHandler img) {
		this.img = img;
	}
	public Ustensil(DataHandler img, String name, int size) {
		super();
		this.img = img;
		this.name = name;
		this.size = size;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}

	public Ustensil() {
		this(null, null ,-1);
	}
	@Override
	public String toString() {
		return "Ustensil [name = " + name + ", size = " + size + "]";
	}
	
}
