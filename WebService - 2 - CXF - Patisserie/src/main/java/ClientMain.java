import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class ClientMain {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		// la aussi plusieurs solutions
		// solution proposé va etre de passer par un objet qui propose tout un tas de chose
		// c'est un obj qui sert à fabriquer des beans
		// il fab des obj: JaxWebServicesFactoryBean
		
		// proxy un objet qui fait semblant d etre un autre objet entre le client et le serv
		// il ne fait que de la communication avec le serveur
		// ça masque la complexité réseau au client
		
		// 1
		JaxWsProxyFactoryBean f = new JaxWsProxyFactoryBean();
		f.setAddress("http://localhost:59054/patisserie");
		f.setServiceClass(PatisserieInterface.class);
		
		// 4 : les intercepteurs
		// séparer en 2 cat : intercepteur en entrée et en sortie
		// un bout de code qui s exec: un peu comme un filtre
		f.getOutInterceptors().add(new LoggingOutInterceptor());
		f.getInInterceptors().add(new LoggingInInterceptor());
		
		PatisserieInterface p = (PatisserieInterface) f.create();
		
		
		// 2 on affiche si le res de la methode creer dans Patisserie par le bias de l'interface
		System.out.println(p.isAvailable("cherry"));
		
		// 3 on affiche les ustensils
		Ustensil [] uTab = p.getUstensils();
		for (Ustensil u: uTab) {
			System.out.println(u);
		}
		
		// 3.1 correction
		for (Ustensil u: p.getUstensils()) {
			System.out.println(u);
			//3.2 Vérifier le type de l'image 3.3 vérifier le nom de l'image
			System.out.println("Nom de l'image: " + u.getImg().getName() +", type img: " + u.getImg().getContentType());
		}
		
		/* 
		 * CCL
		 * 1: p => utiliser les methodes sur le serveur. C'est un appel synchrone: si la méthode met longtemps a renvoyer quelque chose le client est bloqué
		 * 
		 * 2: Serveur = ServeurMain -> Patisserie -> (PatisserieInterface -> Ustensil) 
		 * 		Client = ClientMain -> (Ustensil -> PatisserieInterface)
		 * 
		 *  () = un jar du coté client et du coté serveur
		 * 
		 * en terme de maven
		 * 3 projets
		 * 1: client (ClientMain)
		 * 2: serveur (ServeurMain, Patisserie)
		 * 3: interface
		 * 
		 * 
		 * */
		
	
	}
}
