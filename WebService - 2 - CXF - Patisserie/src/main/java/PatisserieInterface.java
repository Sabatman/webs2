import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface PatisserieInterface {
	public String getError();
	public boolean isAvailable(String nameProd);
	@WebMethod(operationName="isAvailablePw")
	public boolean isAvailable(String nameProd, @WebParam(header=true, name="pwd")String pwd);
	
	public Ustensil[] getUstensils();
}
