import javax.xml.ws.Endpoint;
import javax.xml.ws.soap.SOAPBinding;

public class ServerMain {
	public static void main(String[] args) {
		//Endpoint.publish("http://localhost:8082/patisserie", new Patisserie());
		Endpoint ep = Endpoint.create(new Patisserie());
		SOAPBinding sb = (SOAPBinding) ep.getBinding();
		sb.setMTOMEnabled(true);
		ep.publish("http://localhost:8082/patisserie");
	}
}
