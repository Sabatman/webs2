import java.time.LocalDate;
import java.time.Month;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import javax.jws.WebService;
@WebService
public class Patisserie implements PatisserieInterface{

	String error = null;

	
	@Override
	public String getError() {
		return error;
	}
	
	@Override
	public boolean isAvailable(String nameProd) {
		
		if (nameProd.isEmpty() || (nameProd == null)) {
			error = "empty";
			throw new IllegalArgumentException(error);
		}
		Month today = LocalDate.now().getMonth();
		int m = today.getValue();
		
		if (nameProd.equals("cherry") && ( m < 5 || m > 7))
			return false;
		if (nameProd.equals("strawberry") && ( m < 4 || m > 6))
			return false;
		if (nameProd.equals("raspberry") && ( m < 7 || m > 10))
			return false;
		return true;
	}

	public boolean isAvailable(String nameProd, String pwd) {
		if (!pwd.equals("1234"))
			throw new SecurityException("Le mot de passe doit être un 1234");
		return isAvailable(nameProd);
	}
	@Override
	public Ustensil[] getUstensils() {
		return new Ustensil[] { 
				new Ustensil(new DataHandler(new FileDataSource("img/couteau.jpeg")),"couteau",30), 
				new Ustensil(new DataHandler(new FileDataSource("img/couperet.jpeg")),"couperet",90),
				new Ustensil(new DataHandler(new FileDataSource("img/spatule.jpeg")),"Spatule longue à crêpe",350)};
	}
}
